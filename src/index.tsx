import {
  ApolloClient,
  ApolloLink,
  InMemoryCache,
  HttpLink
} from "apollo-boost";

export function isEmpty(obj: Object) {
  if (Object.keys(obj).length <= 0 || !obj) {
    return true;
  }
  return false;
}
export const configApolloClient = (headersToPass: any, url: string = "") => {
  const headerAlias = "X-Visibility-Scope-Key";
  const httpLink = new HttpLink({
    uri: url || process.env.REACT_APP_SERVER_URL
  });
  if (!isEmpty(headersToPass)) {
    const authLink = new ApolloLink((operation, forward) => {
      const token = headersToPass!.tokenInfo;
      const visibilityKey = headersToPass!.visibilityKey;
      operation.setContext({
        headers: {
          access_token: token ? `${token}` : "",
          [headerAlias]: visibilityKey
        }
      });
      return forward(operation);
    });
    return new ApolloClient({
      link: authLink.concat(httpLink),
      cache: new InMemoryCache({ addTypename: false })
    });
  } else {
    return new ApolloClient({
      link: httpLink,
      cache: new InMemoryCache({ addTypename: false })
    });
  }
};
/**
 * @summary The below function is just to make graphql Query and mutation
 * @param Type: graphql operation (query or mutation)
 * @param Query:actual gql Query consisting response field
 * @param variable: the variable is object i.e passed to query
 * @param header: Used to pass headers like access_token and x visibility scope.
 */
export const graphQlCall = (
  type: string,
  query: any,
  variable?: {},
  headers?: {},
  url?: string
) => {
  let client;
  headers
    ? (client = configApolloClient(headers, url))
    : (client = configApolloClient({}, url));

  switch (type) {
    case "query":
      let queryPromise = client.query({
        query: query,
        variables: { ...variable }
      });

      return queryPromise;
      break;
    case "mutation":
      let mutationPromise = client.mutate({
        mutation: query,
        variables: { ...variable }
      });

      return mutationPromise;
      break;
    default:
      break;
  }
};
/*Below function processes the error occured  during Graphql calls */
export const onGqlError = (error: any) => {
  let errorCount;
  if (error.graphQLErrors.length > 0) {
    errorCount = error.graphQLErrors.map((item: any) => {
      return item.extensions ? item.extensions.error_code : "422"; //To indicate Unprocessable entity (ref: https://www.bennadel.com/blog/2434-http-status-codes-for-invalid-data-400-vs-422.htm)
    });
  } else {
    errorCount = "598"; //Its Used for Network Timeout(ref: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes);
  }
  return errorCount;
};
