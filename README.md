# gql_library

> graph ql functions

[![NPM](https://img.shields.io/npm/v/gql_library.svg)](https://www.npmjs.com/package/gql_library) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save gql_library
```

## Usage

```tsx
import * as React from 'react'

import MyComponent from 'gql_library'

class Example extends React.Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [khushboo-coditas](https://github.com/khushboo-coditas)
